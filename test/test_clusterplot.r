## Test clusterplot
library(tidyverse)
markers <- c("AX-88210599", "AX-88132407", "AX-88173364")
df_clusters <- aqg.toolkit::fetch_clusters(batch = "20190412_Ssa70K_Urke-POX-utbrudd-L17-ordrenr2745_D255_ref2019-442_proj6141",
                            probes = markers, path = "/mnt/project/AquaGen/Rawdata/affy/APT/BATCHES/20190412_Ssa70K_Urke-POX-utbrudd-L17-ordrenr2745_D255_ref2019-442_proj6141")
