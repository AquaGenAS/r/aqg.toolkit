#' Vcf example file
#'
#' The first 5000 rows from Omy50kv2 SNPchip annotation vcf
#'
#' @format A data frame:
#' \describe{
#'   \item{chrom}{Sequnce name}
#'   \item{pos}{The 1-based position}
#'   \item{id}{The identifyer}
#'   \item{ref}{The reference base}
#'   \item{alt}{The laternative base}
#'   \item{qual}{Quality score}
#'   \item{filter}{A flag indicating if the filters have passed}
#'   \item{info}{An extensible list of key-value pairs (fields) describing the variation. See below for some
#'   common fields. Multiple fields are separated by semicolons with optional values in the format: <key>=<data>[,data].}
#' }
"test_vcf"

#' Reads .vcf to data.frame
#'
#' @param x Path to a .vcf file
#'
#' @return A data.frame
#' @importFrom data.table fread
#' @export
#'
#' @examples
read_vcf <- function(x){
  if(grepl(".gz$", x )){
    vcf <- data.table::fread(cmd = paste0("zcat ", x, " | grep -v '^#'"))
  }else{
    vcf <- data.table::fread(cmd = paste0("cat ", x, " | grep -v '^#'"))
  }
  #vcf <- vcf %>% setNames(tolower(gsub("#", "", names(.))))
  colnames(vcf)[1:8] <- c('chrom','pos','id','ref','alt','qual','filter','info')
  return(vcf)
}

#' Parse out keys from the .vcf INFO column
#'
#' @param x A data.frame with a column 'info'
#' @param id The key prefix
#'
#' @return A vector of keys
#' @export
#'
#' @examples
#' data("test_vcf")
#' parse_vcf_info(test_vcf[1:3,], "ID1")
parse_vcf_info <- function(x, id){
  # subf
  subf <-  function(x, id){
    sub1 <- gsub(pattern = paste0(".*",id,"="), replacement = "", x = x)
    sub2 <- gsub(pattern = ";.*", replacement = "", x = sub1)
    return(sub2)
  }
  # vectorize
  subf_v <- Vectorize(subf, USE.NAMES = FALSE)
  # and call
  return(subf_v(x$info, id))
}



