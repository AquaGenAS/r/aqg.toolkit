
`agt.toolkit` is a R package containing a bunch of conveniance functions (for the most part shamelesly stolen from other packages/sites).  <br/>

If you have suggestions for more functions or something is not working: Please write an [issue](https://gitlab.com/AquaGenAS/r/aqg.toolkit/-/issues)

# Installation

Use [devtools](https://devtools.r-lib.org/) for installation 

```r
devtools::install_git("https://gitlab.com/AquaGenAS/r/aqg.toolkit.git")
```

---

# Examples 
Some examples of what you can do:

## Manhatten plot
```r
library(aqg.toolkit)

data(gwas)

# modify the test data so it's ready for plotting; 
#  this is normally done by the functioon read_gcta/gctb ...
test_gwas <- gwas %>% dplyr::group_by(chr) %>%
 dplyr::summarise(chr_len = max(bp)) %>%
 # Calculate cumulative position of each chromosome
 dplyr::mutate(tot=cumsum(chr_len)-chr_len) %>%
 dplyr::select(-chr_len) %>%
 dplyr::right_join(gwas, by = "chr") %>%
 dplyr::arrange(chr, bp) %>%
 dplyr::mutate(bp_cum = bp+tot)

gg_gwas(test_gwas)
```

![Manhatten](test/figs/manhatten.png)

## QQ plot 

```r
data(gwas)

gg_qqplot(gwas$p)+theme_bw()
```

![QQplot](test/figs/QQ.png)

## LD heatmap

Contains some modifications / improvments of the code used by [LDheatmap](https://sfustatgen.github.io/LDheatmap/)

```r
# dummy LD matrix, normally generated with plink or within R 
#  This here represents 10 Snps and 50 animals
myd <- data.frame ( matrix(sample (c(1, 0, -1), 500, replace = TRUE), 50))
mmat <-  abs(cor(myd))

# genrate corresponding GWAS data for 10 snps
gwas <- data.frame(chr = 1,
                   bp = c(1, 10, 15, 18, 20, 23, 24, 30, 35, 40),
                   p  = rnorm(10, 12, 4),
                   snp = colnames(myd),
                   stringsAsFactors = FALSE)

# Draw
plt <- ld_heat(gwas = gwas, ld = mmat)

grid.newpage()
grid.draw(plt)
```

![LDheat](test/figs/LDheat.png)

## Cluster plots

This code is a pure `R` simplification of the code used for `SNPolisher::PsVisualization`. The latter actually uses `perl` to parse the files whereas my code does it in `R`. The results generated - including the confidence ellipses - are identical for both methods. 

If you want to plot ellipses you have to install the [ggforce](https://github.com/thomasp85/ggforce) package.

```r
# locate where the pacakge is installed
data_path <- path.package('aqg.toolkit', quiet = FALSE)

# Reproducable example: Normally there is no need to specify summary, calls and pos
# 
# Normally you would run:
# test_cluster <- get_clusters(batch = <batch_name>, probes = <probe_vector>)
# ... and the function would find your batch in /net/fs-1/projects01/AquaGen/Rawdata/affy/APT/BATCHES
test_cluster <- get_clusters(summary = paste0(data_path, "/data/TEST.AxiomGT1.summary"), 
                             calls =  paste0(data_path, "data/TEST.AxiomGT1.calls.txt"),
                             post = paste0(data_path, "data/TEST.AxiomGT1.snp-posterior.txt"),
                             probes = c("AX-87104774","AX-87104786"))

# make the plot
cp <- ggplot()+
  geom_point(data = test_cluster$calls, 
             aes(x = delta, y = size, color = call2), 
             size = 0.5, alpha = 0.5 )+
  # Optional: plot ellipses with ggforce
  ggforce::geom_ellipse(data = test_cluster$post,  
                        aes( x0 = meanX, y0 = meanY, a = a, b =b, angle = angle, col = call2), 
                        lty =2)+
  facet_wrap(~pcrobeset_id)+
  theme_bw()

# plot
cp
```

![ClusterPlot](test/figs/Cluster_plot.png)


